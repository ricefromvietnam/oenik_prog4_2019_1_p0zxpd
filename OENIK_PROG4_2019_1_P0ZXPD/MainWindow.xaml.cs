﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Media;

namespace OENIK_PROG4_2019_1_P0ZXPD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void PlayBackGroundMusic()
        {
            string url = System.Environment.CurrentDirectory + @"\Music\Victory.mp3";
            backgroundMusic.Source = new Uri(url);
            backgroundMusic.LoadedBehavior = MediaState.Manual;
            backgroundMusic.UnloadedBehavior = MediaState.Manual;
            backgroundMusic.Volume = 1.0;
            backgroundMusic.Position = TimeSpan.Zero;
            backgroundMusic.Play();
        }

        public MainWindow()
        {
            InitializeComponent();
            MapGroup.IsEnabled = false;
            MyPlayer.Opacity = 0;
            PlayBackGroundMusic();
        }

        private void Grid_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Doesn't ready!");
        }

        private void SkipClick(object sender, MouseButtonEventArgs e)
        {
            IntroViewPort.Height = 0;
            MapGroup.IsEnabled = true;
            SkipText.Opacity = 0;
            SkipText.IsEnabled = false;
            MyPlayer.Opacity = 100;
        }
    }
}
