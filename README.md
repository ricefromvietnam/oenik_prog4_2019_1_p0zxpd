# Description

Base: Simple quiz game based on the logic of famous Hungarian Honfoglalo game. The player is able to choose how many player he wants to play against(maximum 3) - right now they will be bots. 

# Game Logic

Before starting the actual game player can set the difficulty level and the chosen topics as well. By default no hardness is selected and all (random) topics included. Quiz will be generated from api json.

The actual game consists of two main round where in the first round the player can occupy territories on the map. The player can occupy only if answer for the question is correct. 

After 10 mins who conquers more area is the winner.

Territories worth points in the range of 200-500 based on the size.

## Item: 

- 50/50: Remove 2 wrong answer in 4.
- ...

# Initial interface

![alt text](interface_example.png?raw=true "Prototype")


